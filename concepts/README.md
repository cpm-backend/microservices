# Microservices Concept

## What is Microservices?

We used to build an app by using monolithic. It's like a big service which contains routing, middlewares, business logic and database access to implement **all features** of our app.

<br/><img src="./screenshots/monolithic.png" width="650"/><br/>

But a single microservice contains routing, middlewares, business logic and database access to implement **one features** of our app. The nice thing about this approach is there are no dependencies between servcies.

<br/><img src="./screenshots/microservices.png" width="700"/><br/>

## Data Management
Data management between services is a very challenging problem of microservices. Because in microservices architecture, each service get its own database (if it needs one), and service will never reach into another services database.

So why we need a database per service? 
- Because we want each service to run independently of other services.
- Sometimes, database schema or structure might change unexpectedly. If two or more services use same database, it's hard to maintain.
- Some services might function more efficiently with different types of databases.

What's the challenge of this architecture?

Imagine we have services A, B, and C with their own database. Now we need a service D to query data that needs to call databases which are owned by service A, B, and C. Owing to the principle that service will never reach into another services database, we cannot query database which is not belong to service D, how should we do?

<br/><img src="./screenshots/data-management.png" width="500"/><br/>

There are 2 general strategies to handle it, *sync* or *async*. Notice that these words don't mean what they mean in the *Javascript* world.

## Sync Communication Strategy

Sync means services communication with each other using **direct requests**. In our example, service D send a request to service A first to get user info. After getting user info, service D uses this data to send another request to service C to get the products ordered by this user. Finally, using product ID to send third request to service B to get detail information about this product.

<br/><img src="./screenshots/sync.png" width="700"/><br/>

Pros of *sync*:
- It's easy to understand.
- Service D won't need a database.

Cons of *sync*:
- It introduces a dependency between services. e.g. If any inter-service request fails, the overall request fail.
- The entire request is only as fast as the slowest request.
- It can easily introduces webs of requests

## Async Communication Strategy

Async means services communication with each other using **events**. A wrong concept is replace the direct request in sync communication with events. Yes, we can call this approach as async communication, but we don't solve any issues in sync communication we mentioned before.

If we want to solve this issue, we can create a database for service D to store this special data structure like so:

<br/><img src="./screenshots/service-d-db.png" width="650"/><br/>

So the challenge is how to store the data to this database. We can create or update it partially when data is created. See the following steps:

1. When we create a product, service B will save this info.
1. Service B will emit an event to *event bus*.
1. Event bus will notify all services (they are all subscribers) after receiving this event.
1. Service D stores this data in its own database after receiving the event from event bus.

Same path with other request like a request to create a user or a request to order a product. The key point is the service will emit an event after handling its part. When service D receive event from event bus, it will create or update data partially.

Pros of *sync*:
- Service D has zero dependencies on other services.
- Service D will be extremely fast.

Cons of *async*:
- There is data duplication. You have to pay for extra storage (but it's still cheap).
- Data might not be consistency.
- It's hard to understand.

We are going to use *async strategy* in this note.
