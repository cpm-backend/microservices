# Mini App

We are going to build a simple mini-microservices app with microservices framework in this tutorial.
<br/><img src="./screenshots/mini-app.png" width="350" /><br/>

## Backend with Node JS
Create a new folder named [blog](./blog/) in current folder. Use following command to get started:
```
mkdir blog
cd blog
npx create-react-app client
```

Create a folder called [posts](./blog/posts/) under blog and initialize the project:
```
mkdir posts
cd posts
yarn init -y
yarn add express cors axios nodemon
```

Go back to blog folder and create a third project called [comments](./blog/comments/):
```
cd ..
mkdir comments
cd comments
yarn init -y
yarn add express cors axios nodemon
```

### Posts Service

<br/><img src="./screenshots/posts.png" width="500" /><br/>
Create a [index.js](./blog/posts/index.js) under `posts` folder like following code. We will store data in memory instead of database in this mini app.
```js
const express = require('express');
const bodyParser = require('body-parser')
const { randomBytes } = require('crypto');

const app = express();
app.use(bodyParser.json());

const posts = {};

app.get('/posts', (req, res) => {
  res.send(posts);
});

app.post('/posts', (req, res) => {
  const id = randomBytes(4).toString('hex');
  const { title } = req.body;
  
  posts[id] = {
    id, title
  };

  res.status(201).send(posts[id]);
});

app.listen(4000, () => {
  console.log('Listening on 4000');
})
```

Add a script in [package.json](./blog/posts/package.json) like so:
```json
{
  "scripts": {
    "start": "nodemon index.js"
  }
}
```

Run `yarn start` then you should see posts service is running in the log. If we want to test our service, we need to use *Postman*. For headers of request, use `Content-Type: application/json`. For body, use following content:
<br/><img src="./screenshots/create-post.png" width="400" /><br/>
You can get all posts like so:
<br/><img src="./screenshots/get-posts.png" width="300" /><br/>

### Comments Service

<br/><img src="./screenshots/comments.png" width="500"/><br/>
Create a [index.js](./blog/comments/index.js) under `comments` folder like posts service. Notice that there are 2 different services, we have to run them in different ports (4000 for posts and 4001 for comments) by each other. After running the service, you can test it by *Postman*:
<br/><img src="./screenshots/create-comment.png" width="400" /><br/>
<br/><img src="./screenshots/get-comments.png" width="400" /><br/>

## React App

In this section, we will build the frontend with React.
<br/><img src="./screenshots/react-app.png" width="650"/><br/>

### Setup
1. Go to [client](./blog/client/) folder.
1. Install `axios`.
    ```
    yarn add axios
    ```
1. Delete all files under [src](./blog/client/src/) folder.
1. Create a file named [App.js](./blog/client/src/App.js) under `src` folder:
    ```js
    import React from 'react';

    export default () => {
      return <div>Blog app</div>;
    }
    ```
1. Create a file named [index.js](./blog/client/src/index.js) under `src` folder:
    ```js
    import React from 'react';
    import ReactDOM from 'react-dom';
    import App from './App';

    ReactDOM.render(
      <App />,
      document.getElementById('root')
    );
    ```
1. Start the React App, you should see the App running on http://localhost:3000.
    ```
    yarn start
    ```

### PostCreate Component
Create a file named [PostCreate.js](./blog/client/src/PostCreate.js) under `src` folder:

```js
import React, { useState } from 'react';
import axios from 'axios';

export default () => {
  const [title, setTitle] = useState('');

  const onSubmit = async event => {
    event.preventDefault();
    await axios.post('http://localhost:4000/posts', {
      title
    });

    // After user submits, the search bar should be empty
    setTitle('');
  };

  return <div>
    <form onSubmit={onSubmit}>
      <div className="form-group">
        <label>Title</label>
        <input
          value={title}
          onChange={e => setTitle(e.target.value)}
          className="form-control">
        </input>
      </div>
      <button className="btn btn-primary">Submit</button>
    </form>
  </div>;
}
```

Update `App.js` like so:
```js
export default () => {
  return (
    <div>
      <h1>Create Post</h1>
      <PostCreate />
    </div>
  );
}
```

Then refresh the page you should see the result. But the default style is not good, we are going to use *BootStrap*. Go to [BootStrap](https://getbootstrap.com/docs/5.0/getting-started/download/), we are going to use the CDN like so:
```html
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
```

Paste above tag in `<header>` of [index.html](blog/client/public/index.html).

Notice that all we need is to use Bootstrap style, we won't use the JS code of BootStrap. We don't need `<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" ...>` tag.

### CORS Policy
When we want to send a request from client (at port 3000) to post service (at port 4000), we have to handle CORS policy which blocks request passed from different domain. We have to add a configuration in our server:
1. Install `cors`.
    ```
    yarn add cors
    ```
1. Update [index.js](blog/posts/index.js) in posts.
    ```js
    const cors = require('cors');
    
    const app = express();
    app.use(cors());
    ```
1. Restart the post service.
    ```
    yarn start
    ```
1. Also do the same steps for comments service.

### PostList and Other Components
Now we want to show posts to users, so we create a [PostList](blog/client/src/PostList.js) like so:

```js
import React, { useState, useEffect } from 'react';
import axios from 'axios';

export default () => {
  const [posts, setPosts] = useState({});

  const fetchPosts = async () => {
    const res = await axios.get('http://localhost:4000/posts');

    setPosts(res.data);
  }

  useEffect(() => {
    fetchPosts();
  }, [])

  const renderedPosts = Object.values(posts).map(post => {
    return <div
      className="card"
      style={{ width: '30%', marginBottom: '20px'}}
      key={post.id}
    >
      <div className="card-body">
        <h3>{post.title}</h3>
      </div>
    </div>
  })

  return <div className="d-flex flex-row flex-wrap justify-content-between">
    {renderedPosts}
  </div>;
};
```

We want to fetch posts when `PostList` is rendered in the first time, so we use `[]` in `useEffect` (state will not be changed, so this method is called only one time).

[CommentCreate](blog/client/src/CommentCreate.js) and [CommentList](blog/client/src/CommentList.js) is similar to PostCreate and PostList.

## Async Communication
Now if we have 3 posts, you will notice that when we load the page first time, browser will send 1 request to get post list and 3 requests to comment by post ID individual. Totally we send 4 requests to server, there are 2 ways to reduce the request times: *sync communication* and *async communication*.

*Sync communication* is easy to understand. For browser, this method is 1 request, but it introduces a dependency between two services. We are going to use *async communication* to solve this problem.
<br/><img src="./screenshots/sync-communication.png" width="700"/><br/>

*Async communication* is more complicated, but it's a better way to solve this issue. Here's step:
1. We create a *event broker* to handle events.
1. When a post is created, `posts` service will emit an event to *event broker*.
    <br/><img src="./screenshots/async-1.png" width="600"/><br/>
1. *Event broker* passes this event to `query` service and stores some parts of information. In this case, `query` service stores `id` and `title` from this event (the info in this event doesn't include `comment` data).
    <br/><img src="./screenshots/async-2.png" width="650"/><br/>
1. When a comment is created, `comments` service will emit an event to *event broker*.
1. *Event broker* passes this event to `query` service and stores `comment`. Because this post ID exists (we don't allow comment without post) in `query` service, all we need to do is update this row by adding `comment` data in it.
    <br/><img src="./screenshots/async-3.png" width="700"/><br/>
1. When we want to get post list including comments, all we need to do is send a request to `query` service and get results.

We can notice that `query` service has zero dependencies on other services, and we don't need to wait the communication between microservices to get final results (we get results from `query` service directly).

But the data is duplication in this method, so it might have consistency issues. Another downside is hard to understand.

Here are some FAQs:

Questions|Answers
--|--
Do we need to create a new service every time we need to join some data?|Absolutely not! In reality, it might not event have posts and comments in separate services in the first place.
Who cares that each service is independent?|*Independent* services and *reliability* that brings is one of the core reasons of using microservices in the first place.
This is so over the top complicated for little benefit.|We will add in some features to get really easy when we use this architecture.

### Event Bus
We call *event broker* as *Event bus* in async communication. There are many different implementations like *RabbitMQ*, *Kafka*, *NATS* and so on. Event bus can receive events and publish events to listener.

Now we are going to build our own event bus using *Express* like so:

<br/><img src="./screenshots/easy-event-bus.png" width="700"/><br/>

When user creates a post, `posts` service will send an request (POST method) to event bus. When event bus receives this request, it will notify all subscribers (`posts`, `comments` and `query` services) by sending POST request for each other.

Here's implementation (refer to [event-bus](blog/event-bus/)):
1. Go to `blog` folder.
1. Create folder named `event-bus` and install packages in it.
    ```
    mkdir event-bus
    cd event-bus
    yarn init -y
    yarn add express nodemon axios
    ```
1. Add a [index.js](blog/event-bus/index.js) like so:
    ```js
    const express = require('express');
    const bodyParser = require('body-parser');
    const axios = require('axios');

    const app = express();
    app.use(bodyParser.json());

    app.post('/events', (req, res) => {
      const event = req.body;

      axios.post('http://localhost:4000/events', event);
      axios.post('http://localhost:4001/events', event);
      axios.post('http://localhost:4002/events', event);

      res.send({ status: 'OK' });
    });

    app.listen(4005, () => {
      console.log('Listening on 4005');
    });
    ```
1. Add a script in [package.json](blog/event-bus/package.json).
    ```json
    {
      "scripts": {
        "start": "nodemon index.js"
      }
    }
    ```
1. Run this service by using command `yarn start`. Now `event-bus` is running at port 4005.

### Emitting Events
Now we have our event bus, next step is to emit events from `posts` and `comments` services to event bus. Furthermore, `posts` and `comments` services also need to handle events passed from event bus.

For `posts` service, we will do the following steps in [index.js](blog/posts/index.js) to send events, same with `comments` service:
1. Add `axios` library.
    ```js
    const axios = require('axios');
    ```
1. Send a request to event bus before responding 201 to browser. Don't forget to use `async-await` context.
    ```js
    app.post('/posts', async (req, res) => {
      ...
      // Pass event to event-bus
      await axios.post('http://localhost:4005/events', {
        type: 'PostCreated',
        data: {
          id, title
        }
      });

      res.status(201).send(posts[id]);
    });
    ```
1. Add code to receive event sent from event bus.
    ```js
    // Receive event but do nothing
    app.post('/events', (req, res) => {
      console.log('Event Received:', req.body.type);
      res.send({});
    });
    ```

### Query Service
It's time to create `query` service, the architecture of `query` service is like so:

<br/><img src="./screenshots/query-service.png" width="650"/><br/>

Follow the steps to create this service:
1. Create a new folder named [query](blog/query/) under `blog` folder and install the packages.
    ```
    mkdir query
    cd query
    yarn init -y
    yarn add express cors nodemon
    ```
1. Add a script in [package.json](blog/query/package.json).
    ```json
    {
      "scripts": {
        "start": "nodemon index.js"
      }
    }
    ```
1. Add a file named [index.js](blog/query/index.js).
    ```js
    const express = require('express');
    const bodyParser = require('body-parser');
    const cors = require('cors');

    const app = express();
    app.use(bodyParser.json());
    app.use(cors());

    const posts = {};

    app.get('/posts', (req, res) => {
      res.send(posts);
    });

    app.post('/events', (req, res) => {
      const { type, data } = req.body;

      if (type === 'PostCreated') {
        const { id, title } = data;
        posts[id] = { id, title, comments: [] };
      }
      if (type === 'CommentCreated') {
        const { id, content, postId } = data;
        const post = posts[postId];
        post.comments.push({ id, content });
      }
      res.send({});
    });

    app.listen(4002, () => {
      console.log('Listening on 4002')
    });
    ```

Now we have `query` service, we don't need to fetch data like previous method. So go back to our [client](blog/client/) and do following refactoring:

1. Go to [PostList.js](blog/client/src/PostList.js), we are going to fetch data from query service.
    ```js
    const fetchPosts = async () => {
      const res = await axios.get('http://localhost:4002/posts');
      setPosts(res.data);
    }
    ```
1. In `PostList.js`, we no longer to pass `postId` to `<CommentList>`. We can pass `comments` directly (we got `comments` from `query` service),
    ```js
    <CommentList comments={post.comments} />
    ```
1. In [CommentList](blog/client/src/CommentList.js), we don't need to send a request to `comments` service for query. We can use `comment` passed from `PostList`.
    ```js
    import React from 'react';

    export default ({ comments }) => {
      const renderComments = comments.map(comment => {
        return <li key={comment.id}>{comment.content}</li>;
      });

      return <ul>{renderComments}</ul>;
    };
    ```

## Moderation Feature
Now we want to add a simple feature in our app to learn more about mechanism in microservices. We want to add in *comment moderation* which flags comments that contain the word *orange*. Here are some clarifications for this feature:
- We don't want to hard code *orange* in our React app.
- Assume that we want to add a new service instead of implementing it in `comments` service.
- It might take a long time for the new service to moderate a comment.

The React app needs to tell the difference between a comment that is pending moderation, rejected, or approved.

<br/><img src="./screenshots/moderation-1.png" width="600"/><br/>
<br/><img src="./screenshots/moderation-2.png" width="650"/><br/>

The first option is to add a new service called `moderation`. This service communicates event creation to query service. See following steps:

<br/><img src="./screenshots/moderation-opt1.png" width="600"/><br/>

1. User submits a comment.
1. `comments` service receive this comment and pass to event bus.
1. Event bus notify all services the event `CommentCreated`.
1. Only `moderation` service cares this event (`query` service doesn't care in this option) and it takes some time to determin add `approved` or `rejected` on `status` property.
1. After `moderation` service finishes, it send a new event `CommentModerated` to event bus.
1. Event bus notify all services the event `CommentModerated`.
1. Now only `query` service cares about this event and stores this data.

The big problem of this method is there is a delay between user submits a comment and the comment is stored in `query` service. Because we have to pass comment through `moderation` service (imagine it might take several minutes), so we cannot see our comment immediately.

The root cause of option 1 is `query` service doesn't care about the event `CommentCreated`. So the option 2 is `query` service cares about this event, it adds a comment record with status `pending`. After `moderation` service finished, event bus notify `query` service to update the status to `approved` or `rejected`.

<br/><img src="./screenshots/moderation-opt2.png" width="600"/><br/>

Option 2 seems to solve this problem, but actually not. `query` service is about presentation logic, in our case it's joining 2 resources (posts and comments), but it might join 10 or more. So the question is: Does it make sense for this presentation service to understand how to process this very precise update? 

Right now, the answer might be yes. Because processing this event is very simple in our case. But in a real world, it might be very complicated, so this kind of presentation service doesn't need to know this very precise update.

### Handling Update
We don't want `query` service to know too detailed update, that's what `comments` service cares about. So here's final solution:
1. User submits a comment.
1. `comments` service receive this comment and store it with `pending` status. After finishing, `comments` service pass `CommentCreated` event to event bus.
1. Event bus notify all services the event `CommentCreated`.
1. Both of `moderation` and `query` service care this event `CommentCreated`. `Query` service stores this comment with status `pending`. When people query this comment now, they can see the status is pending.
1. `Moderation` service handles this event and decide to approve or reject it.
1. After `moderation` service finishes, it sends a new event `CommentModerated` to event bus.
1. Event bus notify all services the event `CommentModerated`.
1. Now only `comments` service cares about this **detailed update** event `CommentModerated` and stores this data.
1. After `comments` service stores the data, it emits a **general event** `CommentUpdated` to event bus.
1. Event bus notify all services the event `CommentUpdated`.
1. Now only `query` service cares about this event `CommentUpdated`, it doesn't need to how or why this comment is changed. All this service to do is to update this data according to this event.

Now `comments` service is in change of detailed update, `query` service just knows the comment is updated.

<br/><img src="./screenshots/moderation-opt3.png" width="700"/><br/>

### Moderation Service
Let's build `moderation` service.
1. Go back to `blog` folder and create moderation project like so.
    ```
    mkdir moderation
    cd moderation
    yarn init -y
    yarn add express axios nodemon
    ```
1. Add a script in [package.json](blog/moderation/package.json) like so:
    ```json
    {
      "scripts": {
        "start": "nodemon index.js"
      }
    }
    ```
1. Create a file named [index.js](blog/moderation/index.js).
    ```js
    const express = require('express')
    const bodyParser = require('body-parser');
    const axios = require('axios');

    const app = express();
    app.use(bodyParser.json());

    app.post('/events', async (req, res) => {
      const { type, data } = req.body;

      if (type === 'CommentCreated') {
        const status = data.content.includes('orange') ? 'rejected' : 'approved';

        await axios.post('http://localhost:4005/events', {
          type: 'CommentModerated',
          data: {
            id: data.id,
            postId: data.postId,
            status,
            content: data.content
          }
        });

        res.send({});
      }
    });

    app.listen(4003, () => {
      console.log('Listening on 4003');
    });
    ```

### Update Services
As our above discussion, we have to do some update within our services, here's the thing we have to do:
- We have to add pending status as default value in `comments` service. Go to `index.js` in `comments` service.
    ```js
    app.post('/posts/:id/comments', async (req, res) => {
      ...
      // Give the pending status as default
      comments.push({ id: commentId, content, status: 'pending' });
      ...
      // Pass status to event bus
      await axios.post('http://localhost:4005/events', {
        type: 'CommentCreated',
        data: {
          id: commentId,
          content,
          postId: req.params.id,
          status: 'pending'
        }
      });
      ...
    });
    ```
- Of course, we have to store this status in `query` service. Go to `index.js` in `query` service.
    ```js
    app.post('/events', (req, res) => {
      ...
      if (type === 'CommentCreated') {
        const { id, content, postId, status } = data;
        const post = posts[postId];
        // Store status
        post.comments.push({ id, content, status });
      }
      ...
    });
    ```
- Don't forget to add `moderation` as a subscriber in event but. Go to `index.js` in `event-bus`.
    ```js
    app.post('/events', (req, res) => {
      ...
      axios.post('http://localhost:4003/events', event);
      ...
    });
    ```
- When `comments` service receives the event `CommentModerated`, it will do some logic and pass new event `CommentUpdated` back to event bus. Go to `index.js` in `comments` service.
    ```js
    app.post('/events', async (req, res) => {
      console.log('Event Received:', req.body.type);

      const { type, data } = req.body;
      if (type === 'CommentModerated') {
        const { postId, id, status, content } = data;
        const comments = commentsByPostId[postId];

        const comment = comments.find(comment => {
          return comment.id === id;
        })
        comment.status = status;

        await axios.post('http://localhost:4005/events', {
          type: 'CommentUpdated',
          data: {
            id,
            status,
            postId,
            content
          }
        });
      }
      res.send({});
    });
    ```
- When `query` service receive event `CommentUpdated`, it needs to update content. Notice that `query` service doesn't care which part of content is updated, it just updates all content. Because `comments` service is in change of that. Go to `index.js` in `query` service.
    ```js
    if (type === 'CommentUpdated') {
      const {id, content, postId, status } = data;
      const post = posts[postId];
      const comment = post.comments.find(comment => {
        return comment.id === id;
      });

      // This service doesn't care about where to be changed, it just updates all data
      comment.status = status;
      comment.content = content;
    }
    ```
- Finally, we update our client to see result. Go to `CommentList.js` in `client`.
    ```js
    export default ({ comments }) => {
      const renderComments = comments.map(comment => {
        let content;

        if (comment.status === 'approved') {
          content = comment.content;
        }
        if (comment.status === 'pending') {
          content = 'This comment is awaiting moderation';
        }
        if (comment.status === 'rejected') {
          content = 'This comment has been rejected';
        }
        return <li key={comment.id}>{content}</li>;
      });
      return <ul>{renderComments}</ul>;
    };
    ```

Now we can test our app. If you want to see the pending status, you can shut down `moderation` service and try to create a new comment. But you notice that when you restart the `moderation` service, this event is still gone. How to fix it?

### Dealing with Missing Events

To deal with missing events, we can store all events in *event bus data store*. If some service is outline, when service restarts then it can retrieve the events within this downtime from event bus.
<br/><img src="./screenshots/event-bus-data-store.png" width=600/><br/>
Now we are going to implement this handler, notice that this is an easy example to help you understand. In real world, this handler is really complicated, we will use framework to do this.

1. Update `index.js` in `event-bus` to store all events and create an endpoint for query.
    ```js
    ...
    const events = [];

    app.post('/events', (req, res) => {
      const event = req.body;
      // Store all events
      events.push(event);
      ...
    });

    // Add an endpoint for query
    app.get('/events', (req, res) => {
      res.send(events);
    })

    app.listen(4005, () => {
      console.log('Listening on 4005');
    });
    ```
1. For `index.js` in `query` service, move logic for handle events to new method called `handleEvent()`. And when `query` service starts, retrieve all events.
    ```js
    app.listen(4002, async () => {
      console.log('Listening on 4002');

      const res = await axios.get('http://localhost:4005/events');

      for (let event of res.data) {
        console.log('Processing event:', event.type);

        handleEvent(event.type, event.data);
      }
    });
    ```

Now you can test it. Please shut down `query` service, then try to do some operations in browser. Then restart the `query` service, it will handle those missing events.

## Deployment

When we deploy our app on virtual machine, we might want to start more instances of a specific microservice to serve our customer. For example, if we want 3 `comments` service to serve our customer, we have to start additional 2 instances of `comments` service. The problem is we have to change the code in event bus to emit events to additional instances. Furthermore, if the instance number is dynamic, it's impossible to change code in event bus depend on that.

<br/><img src="./screenshots/deployment-issue.png" width="650"/><br/>

### Docker

First of all, let's handle the issue of environment. Now running our app makes big assumptions about the environment. e.g. if we don't have `npm` then this service won't start. Another thing is running our app requires precise knowledge of how to start it (`npm start`). That's what *Docker* solves. *Container* wraps up everything that is needed for a program and it also know how to start and run it.
<br/><img src="./screenshots/docker.png" width="550" /><br/>

*Kubernetes* is a tool for running a bunch of different containers. We give it some configuration to describe how we want our containers to run and interact with each other.

See following image. If event bus want to emit events to both `posts` services, it does not need to know the endpoint of them. All it does is to send a request to master and master will forward request to `posts` services.
<br/><img src="./screenshots/kubernetes.png" width="500"/><br/>

Let's make `posts` service run on Docker. Here are steps:
1. Go to `posts` service, create a file [Dockerfile](blog/posts/Dockerfile).

    ```dockerfile
    # Specify base image
    FROM node:alpine

    # Set the working directory to '/app' in the container
    WORKDIR /app

    # Copy over only the package.json file
    COPY package.json ./

    # Install all dependencies
    RUN yarn

    # Copy over all of our remaining source code
    COPY ./ ./

    # Set the command to run when the container starts up
    CMD ["yarn", "start"]
    ```
1. Create another file [.dockerignore](blog/posts/.dockerignore) to ignore the `node_modules/`.
    ```dockerignore
    /node_modules
    ```
1. Start your Docker.
1. Build image of posts.
    ```
    cd posts
    docker build .
    ```
1. Copy the image ID in last console log like `Successfully biult [IMAGE_ID]` after typing previous command.
1. Start `posts` service using this image.
    ```
    docker run [IMAGE_ID]
    ```

Here are some useful `docker` commands:

Commands|Description
--|--
`docker build -t [REPO_NAME]/[IMAGE_NAME] .`|Build an image based on repo name and image name on the `dockerfile` in the current directory. e.g. tag is as `test/posts`.
`docker run [IMAGE_ID or IMAGE_NAME]`|Create and start a container based on the provided image ID or tag.
`docker run -it [IMAGE_ID or IMAGE_NAME] [CMD]`|Create and start container, but also override the default command. e.g. `docker run -it test/posts sh`.
`docker ps`|Printing our information about all of the running containers.
`docker exec -it [CONTAINER_ID] [CMD]`|Execute the given command in a running container.
`docker logs [CONTAINER_ID]`|Print out logs from the given container.
