# Microservices

This is a study note for building 2 apps by microservices framework with *Node JS* and *React*.

There are 3 parts of this note:
1. [Concept](concept/): It introduces what is microservices and when should we use it.
1. [Mini app](mini-app/): We will build a mini app to get a taste of a microservices architecture. **Do not use this project as a template for future microservices**.
1. [Kubernetes](kubernetes/): It introduces *Kubernetes* and we will deploy our mini app on it.

## Reference

[Microservices with Node JS and React](https://www.udemy.com/course/microservices-with-node-js-and-react/).
