# Kubernetes

This section is an extension from [mini-app](../mini-app/). It's recommended to take a look first.

## Get Started

First thing to do is to install *Kubernetes*. If you are using Docker Desktop, all you need to do is go to setting and select Kubernetes then click **Enable Kubernetes**. Apply and restart it then you finish.
<br/><img src="./screenshots/enable-kubernetes.png" width="500"/><br/>
If you are running Docker-Toolbox or Linux, you need to install `minikube`. See more details [here](https://minikube.sigs.k8s.io/docs/start/).

After finishing, please type `kubectl version` on your terminal. You should see **Client Version** and **Server Version**.

Let's see following image to take a look for *Kubernetes* (post and event bus are microservices):
<br/><img src="./screenshots/k8s-overview.png" width="600"/><br/>
- We will get config file into *Kubernetes* using `kubectl`.
- *Kubernetes* will try to find out the image to start the service. If it does not find it in local Docker, it will try to get it from Docker Hub.
- *Kubernetes cluster* includes a collections of nodes and a master to manage them.
- *Node* is a virtual machine that will run containers.
- *Pod* is more or less equal to a running container. Technically, a pod can run multiple containers.
- *Deployment* monitors a set of pods, make sure they are running and restarts them if they crash.
- *Service* provides an easy-to-remember URL to access a running container. So someone want to visit `posts`, it does not need to know the real IP or port, all it needs to do is to send a request to service.

## Pods

Let's start from *Kubernetes config file*. It tells Kubernetes about the different deployment, pods, and services (referred to as *objects*) that we want to create. This file is written in YAML syntax and we always store these files with our project source code (they are documentation).

We can create *objects* without config files, but we should only do this for testing purpose. The best practice is to use config files, because they provide a precise definition of what your cluster is running.

Let's try to use config file to create a pod.
1. Go to [mini-app/blog/posts/](../mini-app/blog/posts/) folder.
    ```
    cd mini-app/blog/posts
    ```
1. Build an image with tag `codeslime/posts:0.0.1` for posts.
    ```
    docker build -t codeslime/posts:0.0.1 .
    ```
1. Go back to `blog` folder and create a file [infra/k8s/posts.yaml](../mini-app/blog/infra/k8s/posts.yaml) (k8s means *Kubernetes*).
    ```yaml
    apiVersion: v1
    kind: Pod
    metadata:
      name: posts
    spec:
      containers:
        - name: posts
          image: codeslime/posts:0.0.1
    ```
1. Run following command to create a pod with this config file.
    ```
    kubectl apply -f posts.yaml
    ```
1. You can check it using following code:
    ```
    kubectl get pods
    ```

Here are some `kubectl` commands about pods:

### Pods Commands

Pods Commands|Description
--|--
`kubectl get pods`|Print out information about all of the running pods.
`kubectl describe pod [POD_NAME]`|Print out details about the running pod.
`kubectl logs [POD_NAME]`|Print out logs from the given pod.
`kubectl exec -it [POD_NAME] [CMD]`|Execute the given command in a running pod.
`kubectl delete pod [POD_NAME]`|Delete the given pod.
`kubectl apply -f [CONFIG_FILE]`|Tell k8s to process the config.

## Deployments

Deployment monitors a set of pods, make sure they are running and restarts them if they crash. Another reason to use deployment is it will handle version update automatically.

Let's create a deployment.
1. Go to `k8s` folder and create a file [posts-version-depl.yaml](../mini-app/blog/infra/k8s/posts-version-depl.yaml) (*depl* means deployment).
    ```yaml
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: posts-depl
    spec:
      replicas: 1
      selector:
        matchLabels:
          app: posts
      template:
        metadata:
          labels:
            app: posts
        spec:
          containers:
            - name: posts
              image: codeslime/posts:0.0.1
    ```
1. Run following command to create a deployment.
    ```
    kubectl apply -f posts-version-depl.yaml
    ```

You can try to delete a pod, you will find deployment will create another pod for you automatically.

### Deployment Commands

Here are some `kubectl` commands about deployments:

Deployments Commands|Description
--|--
`kubectl get deployments`|List all the running deployments
`kubectl describe deployment [DEPL_NAME]`|Print our details about a specific deployment.
`kubectl delete deployment [DEPL_NAME]`|Delete a deployment.
`kubectl apply -f [CONFIG_FILE]`|Create a deployment our of a config file.

### Update Deployment

If we want to update our deployment owing to change of code base (version is changed too), the easy way is to open config file and change the version of image. This is error-prone.

The preferred method is using **latest** tag or don't give it version (it will use **latest** by default). Here are steps:
1. The deployment uses the **latest** tag in the pod spec section. See [posts-depl](../mini-app/blog/infra/k8s/posts-depl.yaml).
1. Make an update to the code.
1. Build the image.
1. Push this image to *Docker Hub*.
    ```
    docker push [IMAGE_NAME]
    ```
1. Run the command to restart.
    ```
    kubectl rollout restart deployment [DEPL_NAME]
    ```

## Services

*Service* provides an easy-to-remember URL to access a running container, which means services provide networking between pods.
. So we don't need to remember to name or IP address of pod generated by deployment.

There are 4 types of services:

Types|Usage
--|--
Cluster IP|It sets up an easy-to-remember URL to access a pod. It only exposes pods **in the cluster**.
Node Port|It makes a pod accessible from **outside the cluster**. Usually only used for **dev** purposes.
Load Balancer|It makes a pod accessible from **outside the cluster**. This is the right way to expose a pod to the outside world.
External Name|(Advanced) It redirects an in-cluster request to a CNAME URL.

### Node Port Services
If we don't need communication between pods (e.g. only one serivce), we can use node port as service. Here are steps to create *node port* service.
1. Go to `k8s` folder and create a file [posts-srv.yaml](../mini-app/blog/infra/k8s/posts-srv.yaml) (*srv* means service).
    ```yaml
    apiVersion: v1
    kind: Service
    metadata:
      name: posts-srv
    spec:
      type: NodePort
      selector:
        app: posts
      ports: 
        - name: posts
          protocol: TCP
          port: 4000
          targetPort: 4000

    ```
1. Run following command to create a service.
    ```
    kubectl apply -f posts-srv.yaml
    ```
1. You can check your service now.
    ```
    kubectl get services
    ```
When you type above command on your terminal, you will get the port information like `4000:30497/TCP`. The `4000` is *port*, `30497` is *node port*. You might notice that we have *target port* in our config file. What are difference?

Node port is for real world communication, it's generated by k8s randomly. So if we want to visit this service locally, we have to visit http://localhost:30497/posts like so. Port is for service, target port is for pod. See the following image:
<br/><img src="./screenshots/port-targetport.png" width="650"/><br/>

### Cluster IP Services
If we have 2 or more pods and they have to communicate with each other (communication is in the cluster), we have to use cluster IP services. We will use posts and event bus services as example in this section. 

Post pod cannot send a request to event bus pod directly, post has to send a request to event bus service first. That's why we need to use cluster IP service.

Do the following steps to start event bus:
1. Go to `event-bus` folder and create an image.
    ```
    cd event-bus
    docker build -t codeslime/event-bus .
    ```
1. Push image to your *Docker Hub*.
    ```
    docker push codeslime/event-bus
    ```
1. Go to `k8s` folder and create a config file [event-bus-depl.yaml](../mini-app/blog/infra/k8s/event-bus-depl.yaml).
    ```yaml
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: event-bus-depl
    spec:
      replicas: 1
      selector:
        matchLabels:
          app: event-bus
      template:
        metadata:
          labels:
            app: event-bus
        spec:
          containers:
            - name: event-bus
              image: codeslime/event-bus
    ```
1. Create a deployment for event bus and posts.
    ```
    kubectl apply -f event-bus-depl.yaml
    kubectl apply -f posts-depl.yaml
    ```
1. Now you should see 2 running pods.
    ```
    kubectl get pods
    ```

We can put more than one objects in single YAML file. In this case, the pod (deployment) and service is 1-to-1 mapping, so it's good to put service and deployment in one single YAML. But it's fine to separate them to different files. Update [event-bus-depl.yaml](../mini-app/blog/infra/k8s/event-bus-depl.yaml) to add service config like so:
```yaml
...
## Service
---
apiVersion: v1
kind: Service
metadata:
  name: event-bus-srv
spec:
  #type: ClusterIP
  selector:
    app: event-bus
  ports:
    - name: event-bus
      protocol: TCP
      port: 4005
      targetPort: 4005
```

Notice that cluster IP service is a default type, so you don't need to specify it.

Now run following command to apply this config again. Then, check if we have 3 services now (`kubernetes` is default service).
```
kubectl apply -f event-bus-depl.yaml
kubectl get services
```

Now we will create a cluster IP service for posts, we do the same thing to put service in deployment file [posts-depl.yaml](../mini-app/blog/infra/k8s/posts-depl.yaml). Then apply this file again.
```
kubectl apply -f posts-depl.yaml
kubectl get services
```

The endpoint of service is service name. So if posts service wants to send a request to event bus, the URL is http://event-bus-srv:4005/. You can check service name and port by using command `kubectl get services`.
<br/><img src="./screenshots/clusterip.png" width="500"/><br/>

So we update the URL in [event-bus/index.js](../mini-app/blog/event-bus/index.js) (you need to comment other services) and [posts/index.js](../mini-app/blog/posts/index.js) like http://posts-clusterip-srv:4000/events and http://event-bus-srv:4005/events.

After updating services, you can test it by using Postman. Notice that the localhost port is provided by node port service, you have to check it by `kubectl get services`. Your port might not be 30497.
<br/><img src="./screenshots/create-post.png" width="350"/><br/>

Final step is to take a look for pod's log, if event bus can communicate with posts, you should the log `Event Received: PostCreated`.

Do the same thing with comments, query and moderation service. You can test functionality using Postman.

### Load Balancer Services

Let's handle frontend now. Just like other microservices, we will run *React* server in a pod. Notice that the only goal of this dev server is to take code to generate some HTML, JS and CSS files out of it. **What this *React* server is not doing is making any requests at any point in time**.
<br/><img src="./screenshots/react-server.png" width="600"/><br/>

**The actual requests for data are being issued from the user's browser**. We are going to create a *load balancer* service. The goal of this service is to have one single point of entry into our entire cluster. We write some logic in this service and route these incoming requests off to the appropriate pod. 
<br/><img src="./screenshots/load-balancer.png" width="650"/><br/>

There are 2 terms that you might confuse. The goal of load balancer is to get traffic in to a **single pod**. *Ingress controller* is a pod with a set of routing rules to distribute traffic to other services.

When we use config to create load balancer service, actually we ask cloud provider (e.g. AWS, GCP, Asure) to create a load balancer completely **outside** of our cluster. Load balancer take a request from outside world and directly send it to *some pod* in our cluster.
<br/><img src="./screenshots/ingress-1.png" width="500"/><br/>

This *some pod* is so-called *ingress controller*. Ingress controller will distribute traffic to other services.
<br/><img src="./screenshots/ingress-2.png" width="600"/><br/>

We are going to install [ingress-nginx](https://kubernetes.github.io/ingress-nginx/). It's a project that will create a load balancer service and an ingress for us. Another project called `kubernetes-ingress` does the same thing with `ingress-nginx`. Now check the official [document](https://kubernetes.github.io/ingress-nginx/deploy/#provider-specific-steps) to install `ingress-nginx`.

Let's create ingress controller.
1. Go to `k8s` folder and create a file [ingress.srv](../mini-app/blog/infra/k8s/ingress-srv.yaml).
    ```yaml
    apiVersion: networking.k8s.io/v1beta1
    kind: Ingress
    metadata:
      name: ingress-srv
      annotations:
        kubernetes.io/ingress.class: nginx
    spec:
      rules:
        - host: posts.com
          http:
            paths:
            - path: /posts
                backend:
                  serviceName: posts-clusterip-srv
                  servicePort: 4000
    ```
1. Create this ingress controller.
    ```
    kubectl apply -f ingress.srv
    ```
1. If you want to test it locally, you have to set up host file. For macOS, open `/etc/hosts` and add one line at the bottom of file and save it like so:
    ```
    127.0.0.1 posts.com
    ```
1. Visit http://posts.com/posts, you will get results. If you are unable to access the application you might have something already running on port 80, which is the default port for the ingress. You can check it as follows:
    ```
    sudo lsof -i tcp:80
    ```

Notice that the different domains can be hosted inside of one single k8s cluster. Ingress ngnix is setup assuming that you might be hosting many different apps at different domains.

Now we can deploy our *React* app.
1. Go to `client` folder and replace `localhost:4000` with `posts.com` in all files under `src` ([CommentCreate.js](../mini-app/blog/client/src/CommentCreate.js), [PostCreate.js](../mini-app/blog/client/src/PostCreate.js), and [PostList.js](../mini-app/blog/client/src/PostList.js)).
1. Build an image for client.
    ```
    docker build -t codeslime/client .
    ```
1. Push image to your *Docker Hub*.
    ```
    docker push codeslime/client
    ```
1. Go to `k8s` folder and create a config file [client-depl.yaml](../mini-app/blog/infra/k8s/client-depl.yaml).
    ```yaml
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: client-depl
    spec:
      replicas: 1
      selector:
        matchLabels:
          app: client
      template:
        metadata:
          labels:
            app: client
        spec:
          containers:
            - name: client
              image: codeslime/client
    ## Service
    ---
    apiVersion: v1
    kind: Service
    metadata:
      name: client-srv
    spec:
      selector:
        app: client
      ports:
        - name: client
          protocol: TCP
          port: 3000
          targetPort: 3000
    ```
1. Start *React* with config file.
    ```
    kubectl apply -f client-depl.yaml
    ```

In above steps, you notice that we have 2 same path with different methods. One is `POST /posts` to create a post, another is `GET /posts` to query posts. The bad news is `ingress-nginx` module cannot do routing based upon the method of the request. So only thing we can really route effectively is a path. So we change path for creating a post to `POST /posts/create`.
1. Go to [PostCreate.js](../mini-app/blog/client/src/PostCreate.js) and update create a post path to `/posts/create`.
    ```js
    const onSubmit = async (event) => {
        event.preventDefault();
        await axios.post("http://posts.com/posts/create", { title });
        ...
    };
    ```
1. Go to [posts/index.js](../mini-app/blog/posts/index.js) to update path for this API.
    ```js
    app.post("/posts/create", async (req, res) => {
        ...
    };
    ```
1. Rebuild the images for `client` and `posts`. Then push to *Docker Hub*.
    ```
    cd client
    docker build -t codeslime/client .
    docker push codeslime/client
    cd ../posts
    docker build -t codeslime/posts .
    docker push codeslime/posts
    ```
1. Restart both of services.
    ```
    kubectl rollout restart deployment client-depl
    kubectl rollout restart deployment posts-depl
    ```
1. Add paths to [ingress-srv.yaml](../mini-app/blog/infra/k8s/ingress-srv.yaml).
    ```yaml
    apiVersion: networking.k8s.io/v1beta1
    kind: Ingress
    metadata:
      name: ingress-srv
      annotations:
        kubernetes.io/ingress.class: nginx
        # Use regular expression
        nginx.ingress.kubernetes.io/use-regex: 'true'
    spec:
      rules:
        - host: posts.com
          http:
            paths:
              - path: /posts/create
                backend:
                  serviceName: posts-clusterip-srv
                  servicePort: 4000
              - path: /posts
                backend:
                  serviceName: query-srv
                  servicePort: 4002
              # You need to use regular expression instead of wildcard
              - path: /posts/?(.*)/comments
                backend:
                  serviceName: comments-srv
                  servicePort: 4001
              - path: /?(.*)
                backend:
                  serviceName: client-srv
                  servicePort: 3000
    ```
1. Apply ingress config file.
    ```
    cd blog/infra/k8s
    kubectl apply -f ingress-srv.yaml
    ```
1. Visit http://posts.com/, you should see your app.

### Skaffold

You might notice that if we change the code every time, we have build and push the image then restart it. It's really annoying. *Skaffold* is what we need. *Skaffold* is a command line tool to automate many tasks in a k8s dev environment.

Go to official website [Skaffold](https://skaffold.dev/docs/install/) to find the installation document.

```
brew install skaffold
```

Create a setup file for Skaffold in the mini-app project. Go to `blog` folder, create a file [skaffold.yaml](../mini-app/blog/skaffold.yaml).
```yaml
apiVersion: skaffold/v2alpha3
kind: Config
deploy:
  kubectl:
    manifests:
      - ./infra/k8s/*
build:
  local:
    push: false
  artifacts:
    - image: codeslime/client
      context: client
      docker:
        dockerfile: Dockerfile
      sync:
        manual:
          - src: 'src/**/*.js'
            dest: .
    ...
```

Notice that the written in `skaffold.yaml` will not get applied to k8s. Instead it is just consumed by *Skaffold*.
- `deploy.kubectl.manifests='./infra/k8s/*'` means Skaffold are watching those files. When we start up Skaffold, Skaffold apply these files. When we make a change, it also applies these files. When we stop, Skaffold find all the *objects* related to these config file and delete them (does not delete file).
- When default Skaffold makes a change to our image or rebuild an image that's going to push it up to *Docker Hub*. That's not required when we are using Scaffold so we disable that default behavior by using `build.local.push=false`.
- For `artifacts` settings, it means Skaffold will watch `client` folder in our code. 
    - If changed files are matched to `src/**/*.js` then it will take that changed file and directly throw it into our pod.
    - If changed files are not matched up by `src/**/*.js`, Skaffold will try to rebuild the entire image.

Finally, go to `blog` folder and run Skaffold file. You might see some error message in first time. When it finishes, try to run following command second time.
```
skaffold dev
```

Now you can visit http://posts.com again. Try to update your source code like change the title, then refresh the page you should see the update.

> ### *Note*
> If you just only chagne JS file, all *Skaffold* does is to send updated file into your pod. You still need a tool to monitor this change which is `nodemond`. It's like 2 layer restart. So if you don't use tool to monitor the change, *Skaffold* is not going to work.
